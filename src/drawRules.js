// setZeroTimeout impl
(function() {
    let timeouts = [];
    let messageName = "zero-timeout-message";

    function setZeroTimeout(fn) {
        timeouts.push(fn);
        window.postMessage(messageName, "*");
    }

    function handleMessage(event) {
        if (event.source === window && event.data === messageName) {
            event.stopPropagation();
            if (timeouts.length > 0) {
                let fn = timeouts.shift();
                fn();
            }
        }
    }

    window.addEventListener("message", handleMessage, true);

    window.setZeroTimeout = setZeroTimeout;
})();

let context;
let contextForeground = "#000";
let contextBackground = "#fff";
let canvasWidth = 1200;
let canvasHeight = 600;
let canvas;

window.onload = function () {
    let canvasDiv = document.getElementById("canvasDiv");
    canvas = document.createElement('canvas');
    canvas.setAttribute("width", canvasWidth);
    canvas.setAttribute("height", canvasHeight);
    canvas.setAttribute("id", "canvas");
    canvasDiv.appendChild(canvas);
    context = canvas.getContext("2d");
};

function changeCanvasAttributes(w, h, f, b) {
    canvasWidth = parseInt(w);
    canvasHeight = parseInt(h);
    contextForeground = f;
    contextBackground = b;
    canvas.setAttribute("width", canvasWidth);
    canvas.setAttribute("height", canvasHeight);
    updateImage();
}

function saveImage() {
    window.open(canvas.toDataURL());
}

// Rule drawing (cool rules: 149, 82 - pattern4: 35124r, 1244d, 13443)

function Rule(number, cellSize) {
    this.number = number;
    this.cellSize = cellSize;
    this.patterns = [];

    for (let i = 0; i < Math.pow(2, cellSize); i += 1) {
        let state = number % 2;
        this.patterns[i] = state;
        if (state === 1) {
            number -= 1;
        }
        number /= 2;
    }

    this.getState = function (pattern) {
        let index = pattern.reduce((res, x) => res << 1 | x);
        return this.patterns[index];
    }
}

function dot(x, y, dim) {
    context.fillRect(x * dim, y * dim, dim, dim);
}

let y;
let timer;
let currentRow;
let nextRow;
let rule;
let zoomFactor;
let gridX;
let gridY;
let ySingleRule;
let gridYSingleRule;

function resetCanvas() {
    context.clearRect(0, 0, canvasWidth, canvasHeight);
    context.fillStyle = contextBackground;
    context.fillRect(0, 0, canvasWidth, canvasHeight);
    context.fillStyle = contextForeground;
}

function drawRow(row, yPos) {
    row.forEach((e, i) => {
        if (e) {
            dot(i + 1, yPos, zoomFactor);
        }
    });
}

function timerCallback() {
    if (y >= gridY) {
        stopDrawRule();
        return;
    }

    y += 1;

    for (let x = 0; x < gridX; x += 1) {
        if (x + rule.cellSize - 1 < gridX) {
            let pattern = currentRow.slice(x, x + rule.cellSize);
            const nextX = x + 1;
            nextRow[nextX] = rule.getState(pattern);
        }
    }

    drawRow(nextRow, y);

    currentRow = nextRow;
    nextRow = [];

    for (let i = 0; i < gridX; i += 1)
        nextRow[i] = 0;

    setZeroTimeout(timerCallback);
}

function startDrawRule(_cellSize, _ruleNumber, _startingDots, _zoomFactor) {
    stopDrawRule();
    resetCanvas();
    executionTime("start");
    y = 0;
    zoomFactor = parseInt(_zoomFactor);
    gridX = Math.round(canvasWidth / zoomFactor);
    gridY = Math.round(canvasHeight / zoomFactor);

    const startingDots = parseInt(_startingDots);
    if (startingDots <= 0) {
        for (let i = 0; i < gridX; i += 1) {
            currentRow[i] = Math.random() < 0.5 ? 0 : 1;
            nextRow[i] = 0;
        }
    } else {
        let step = Math.round(gridX / (startingDots + 1));
        for (let i = 0; i < gridX; i += 1) {
            currentRow[i] = i % step === 0 ? 1 : 0;
            nextRow[i] = 0;
        }
    }

    currentRow[0] = 0;
    drawRow(currentRow, 0);

    rule = new Rule(parseInt(_ruleNumber), parseInt(_cellSize));

    setZeroTimeout(timerCallback);
}

function stopDrawRule() {
    clearTimeout(timer);
    timer = null;
    y = 0;
    currentRow = [];
    nextRow = [];
    rule = null;
    gridX = canvasWidth;
    gridY = canvasHeight;
    executionTime("stop");
    updateImage();
}

function updateImage() {
    document.getElementById("canvasImage").src = canvas.toDataURL();
}

function startDrawAllRules(_cellSize) {
    stopDrawRule();
    resetCanvas();
    executionTime("start");
    y = 0;
    zoomFactor = 1;
    gridX = Math.round(canvasWidth / zoomFactor);
    gridY = Math.round(canvasHeight / zoomFactor);
    ySingleRule = 0;
    gridYSingleRule = 30;

    for (let i = 0; i < gridX; i += 1) {
        currentRow[i] = Math.random() < 0.5 ? 0 : 1;
        nextRow[i] = 0;
    }

    drawRow(currentRow, 0);

    rule = new Rule(0, parseInt(_cellSize));

    setZeroTimeout(timerCallback2);
}

function drawRuleNumber() {
    context.fillStyle = "#fff";
    context.fillRect(0, y - 25, 60, 20);
    context.fillStyle = "#000";
    context.fillRect(1, y - 24, 58, 18);
    context.fillStyle = "#fff";
    context.fillRect(2, y - 23, 56, 16);
    context.fillStyle = "#000";
    context.fillText(rule.number, 10, y - 10);
}

function changeRule() {
    clearTimeout(timer);
    timer = null;

    ySingleRule = 0;

    for (let i = 0; i < gridX; i += 1) {
        currentRow[i] = Math.random() < 0.5 ? 0 : 1;
        nextRow[i] = 0;
    }

    rule = new Rule(rule.number + 1, rule.cellSize);
    setZeroTimeout(timerCallback2);
}

function timerCallback2() {
    if (y >= gridY) {
        drawRuleNumber();
        stopDrawRule();
        return;
    }

    y += 1;

    if (ySingleRule < gridYSingleRule) {
        for (let x = 0; x < gridX; x += 1) {
            if (x + rule.cellSize - 1 < gridX) {
                let pattern = currentRow.slice(x, x + rule.cellSize);
                const nextX = x + 1;
                nextRow[nextX] = rule.getState(pattern);
            }
        }

        drawRow(nextRow, y);

        currentRow = nextRow;
        nextRow = [];

        for (let i = 0; i < gridX; i += 1)
            nextRow[i] = 0;

        ySingleRule += 1;

        setZeroTimeout(timerCallback2);
    } else {
        drawRuleNumber();
        changeRule();
    }
}

function executionTime(start_stop) {
    let d = new Date();
    let execution_time = document.getElementById("execution_time");

    if (start_stop === "start") {
        startTime = d.getTime();
        execution_time.innerHTML = "Started!";
    }

    if ((start_stop === "stop") && (typeof(startTime) !== "undefined")) {
        execution_time.innerHTML = (d.getTime() - startTime) + " ms";
    }
}
